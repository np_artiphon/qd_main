1. install arduino ide 1.8.7
2. install teensyduino 1.44
3. verify martin's sample sketch compiles
3a. install adafruit DRV 2605 library - https://github.com/adafruit/Adafruit_DRV2605_Library
4. Should compile with no issues and open teensy loader
5. Download / install Sloeber
6. Open Arduino->Preferences
6a. Add teensy library path to private library path  (e.g. C:\Program Files (x86)\Arduino\hardware\teensy\avr\libraries)
6b. Add Teensy hardware path (e.g. C:\Program Files (x86)\Arduino\hardware\teensy\avr)


7. Should be able to build (CTRL+B) with Sloeber
Note that a clean build on my (nick) machine seems to take multiple passes? It will fail the first time or two with library issues, but rebuilding fixes it. 
