#include <Audio.h>
#include <Wire.h>
#include <SPI.h>
#include <SD.h>
#include <SerialFlash.h>
#include <Bounce.h>

// GUItool: begin automatically generated code
AudioSynthNoisePink      kickNoise;      //xy=1200,2092.5
AudioSynthSimpleDrum     kickDrum;       //xy=1214,1987.5
AudioSynthWaveformSineModulated kickFM;         //xy=1224,2201.5
AudioSynthNoiseWhite     snare;          //xy=1327,2374.5
AudioSynthWaveformDc     snareEnv;       //xy=1332,2415.5
AudioSynthNoiseWhite     noise;          //xy=1363,1677.5
AudioSynthWaveformDc     hhEnv;          //xy=1377,2565.5
AudioMixer4              kickPreMix;     //xy=1379,2150.5
AudioSynthNoiseWhite     hh;             //xy=1379,2529.5
AudioSynthWaveform       padOneWavTwo;   //xy=1448,1464.5
AudioSynthWaveform       padOneWavOne;   //xy=1452,1427.5
AudioSynthWaveform       padTwoWavTwo;   //xy=1462,1886.5
AudioSynthWaveform       padTwoWavOne;   //xy=1463,1851.5
AudioEffectMultiply      kickEnv;        //xy=1479,2090.5
AudioSynthSimpleDrum     snareDrum;      //xy=1484,2433.5
AudioEffectMultiply      snareDriver;    //xy=1485,2389.5
AudioEffectMultiply      hhDriver;       //xy=1511,2547.5
AudioMixer4              kickMixer;      //xy=1581,2228.5
AudioSynthKarplusStrong  string1;        //xy=1590,1599.5
AudioSynthKarplusStrong  string2;        //xy=1591,1640.5
AudioMixer4              oscOneMixer;    //xy=1633,1464.5
AudioMixer4              oscTwoMixer;    //xy=1634,1867.5
AudioSynthKarplusStrong  string3;        //xy=1639,2021.5
AudioSynthKarplusStrong  string4;        //xy=1643,2060.5
AudioFilterStateVariable hhFilter;       //xy=1642,2565.5
AudioSynthWaveformDc     envOne;         //xy=1663,1529.5
AudioSynthWaveformDc     envTwo;         //xy=1677,1932.5
AudioMixer4              stringMixer;    //xy=1753,1633.5
AudioMixer4              drumMixer;      //xy=1778,2261.5
AudioEffectMultiply      multiply1;      //xy=1798,1483.5
AudioEffectMultiply      multiply2;      //xy=1797,1887.5
AudioMixer4              stringMixerTwo; //xy=1800,2059.5
AudioSynthWaveform       lfo;            //xy=1941,1813.5
AudioSynthWaveform       drumLFO;        //xy=1981,2325.5
AudioSynthWaveformDc     lfoDC;          //xy=1994,1734.5
AudioMixer4              oscStringMixerTwo; //xy=2011,1958.5
AudioMixer4              oscStringMixer; //xy=2028,1592.5
AudioFilterStateVariable mainDrumFilter; //xy=2165,2200.5
AudioMixer4              lfoMixer;       //xy=2241,1792.5
AudioFilterStateVariable padOneFilter;   //xy=2290,1569.5
AudioAnalyzePeak         lfoPeak;        //xy=2299,1704.5
AudioFilterStateVariable padTwoFilter;   //xy=2302,1926.5
AudioAnalyzePeak         peak2;          //xy=2438,2026.5
AudioAnalyzePeak         peak1;          //xy=2471,1495.5
AudioFilterStateVariable delayFilter;    //xy=2503,1838.5
AudioMixer4              delayMixStage;  //xy=2552,1750.5
AudioEffectDelay         delay1;         //xy=2622,2046.5
AudioMixer4              finalStage;     //xy=2774,1820.5
AudioOutputI2S           i2s2;           //xy=2929,1818.5
AudioConnection          patchCord1(kickNoise, 0, kickPreMix, 0);
AudioConnection          patchCord2(kickDrum, 0, kickEnv, 0);
AudioConnection          patchCord3(kickDrum, 0, kickMixer, 0);
AudioConnection          patchCord4(kickFM, 0, kickPreMix, 1);
AudioConnection          patchCord5(snare, 0, snareDriver, 0);
AudioConnection          patchCord6(snareEnv, 0, snareDriver, 1);
AudioConnection          patchCord7(noise, 0, oscOneMixer, 2);
AudioConnection          patchCord8(noise, 0, oscTwoMixer, 2);
AudioConnection          patchCord9(hhEnv, 0, hhDriver, 1);
AudioConnection          patchCord10(kickPreMix, 0, kickEnv, 1);
AudioConnection          patchCord11(hh, 0, hhDriver, 0);
AudioConnection          patchCord12(padOneWavTwo, 0, oscOneMixer, 1);
AudioConnection          patchCord13(padOneWavOne, 0, oscOneMixer, 0);
AudioConnection          patchCord14(padTwoWavTwo, 0, oscTwoMixer, 1);
AudioConnection          patchCord15(padTwoWavOne, 0, oscTwoMixer, 0);
AudioConnection          patchCord16(kickEnv, 0, kickMixer, 1);
AudioConnection          patchCord17(kickEnv, kickFM);
AudioConnection          patchCord18(snareDrum, 0, drumMixer, 2);
AudioConnection          patchCord19(snareDriver, 0, drumMixer, 1);
AudioConnection          patchCord20(hhDriver, 0, hhFilter, 0);
AudioConnection          patchCord21(kickMixer, 0, drumMixer, 0);
AudioConnection          patchCord22(string1, 0, stringMixer, 0);
AudioConnection          patchCord23(string2, 0, stringMixer, 1);
AudioConnection          patchCord24(oscOneMixer, 0, multiply1, 0);
AudioConnection          patchCord25(oscTwoMixer, 0, multiply2, 0);
AudioConnection          patchCord26(string3, 0, stringMixerTwo, 0);
AudioConnection          patchCord27(string4, 0, stringMixerTwo, 1);
AudioConnection          patchCord28(hhFilter, 1, drumMixer, 3);
AudioConnection          patchCord29(envOne, 0, multiply1, 1);
AudioConnection          patchCord30(envTwo, 0, multiply2, 1);
AudioConnection          patchCord31(stringMixer, 0, oscStringMixer, 1);
AudioConnection          patchCord32(drumMixer, 0, mainDrumFilter, 0);
AudioConnection          patchCord33(multiply1, 0, oscStringMixer, 0);
AudioConnection          patchCord34(multiply2, 0, oscStringMixerTwo, 0);
AudioConnection          patchCord35(stringMixerTwo, 0, oscStringMixerTwo, 1);
AudioConnection          patchCord36(lfo, 0, padOneFilter, 1);
AudioConnection          patchCord37(lfo, 0, padTwoFilter, 1);
AudioConnection          patchCord38(lfo, 0, lfoMixer, 0);
AudioConnection          patchCord39(drumLFO, 0, mainDrumFilter, 1);
AudioConnection          patchCord40(lfoDC, 0, lfoMixer, 1);
AudioConnection          patchCord41(oscStringMixerTwo, 0, padTwoFilter, 0);
AudioConnection          patchCord42(oscStringMixer, 0, padOneFilter, 0);
AudioConnection          patchCord43(mainDrumFilter, 0, delayMixStage, 2);
AudioConnection          patchCord44(lfoMixer, lfoPeak);
AudioConnection          patchCord45(padOneFilter, 0, delayMixStage, 0);
AudioConnection          patchCord46(padOneFilter, 0, peak1, 0);
AudioConnection          patchCord47(padTwoFilter, 0, delayMixStage, 1);
AudioConnection          patchCord48(padTwoFilter, 0, peak2, 0);
AudioConnection          patchCord49(delayFilter, 0, delayMixStage, 3);
AudioConnection          patchCord50(delayMixStage, delay1);
AudioConnection          patchCord51(delayMixStage, 0, finalStage, 0);
AudioConnection          patchCord52(delay1, 0, delayFilter, 0);
AudioConnection          patchCord53(finalStage, 0, i2s2, 0);
AudioConnection          patchCord54(finalStage, 0, i2s2, 1);
AudioControlSGTL5000     sgtl5000_1;     //xy=2712,1640.5
// GUItool: end automatically generated code

#include "Adafruit_DRV2605.h"
Adafruit_DRV2605 drv;
int vibes = 0;

#include <Metro.h>
Metro serialMetro = Metro(500); // metro is a timer library. sequencer fixed at 500ms = 120bpm

#include <NXPMotionSense.h> //mv IMU
#include <EEPROM.h> //mv IMU
NXPMotionSense imu; //mv IMU
NXPSensorFusion filter; //mv IMU
float ax, ay, az; //mv IMU
float gx, gy, gz; //mv IMU
float mx, my, mz; //mv IMU
float roll, pitch, heading; //mv IMU
int noteVal; //mv IMU note control
float drumVol; //mv IMU drum control
int baseline = 2000000000;//mv capsense declare as high value.
int maximum;//mv capsense


float analogValues[13];
float analogValuesLag[13];
int digitalValues[16];
int prevDigitalValues[16];
int btnState;
int PrevBtnState;
Bounce wavTrig = Bounce(0,5); //waveform select button is also on pin 28

//SynthVars
int glideTime;
float detune;
float attackTime;
float decayTime;

int octIndex;
float octArray[5] = {.25,.5,1,2,4}; //sets the scaling of the notes when the octave knob is turned
float octave;

float padOnePeak;
float padTwoPeak;

int wavIndex = 0;
short waveShapes[4] = {
  WAVEFORM_TRIANGLE,
  WAVEFORM_SAWTOOTH,
  WAVEFORM_SQUARE,
  WAVEFORM_SQUARE,
};

bool padOneNoteOn;
bool padTwoNoteOn;

bool yPosAlt;
float yPosBendOne = 1;
float yPosBendTwo = 1;

bool firstRun;

//freq
int quantizedValue;
int prevQuantizedValue;
float padOneCurrentXVal;
float padOnePrevXVal;
float padTwoCurrentVal;
float padTwoPrevVal;
float aMinor[8]{
    220.00,246.94,261.63,293.66,
    329.63,349.23,394.00,440.00
};

float lfoPeakVal;

unsigned long mv_updatePrev; //mv counter for debug;

//DRUMS!!!
bool drumMode;
int seqStep;
int prevSeqStep;

bool kickSteps[16]{false};
bool snareSteps[16]{false};
bool hhSteps[16]{false};

int snareRelease;
int hhRelease;
bool seqPlay;
float seqInterval;
int seqPage;
int lfoIndex;
int drumLfoIndex;
int lfoTimeMathArray[5] = {250, 500, 1000, 2000, 4000};

//DrumAutomation
int kickPitch[16];
int kickPitchFM[16];
int kickLength[16];
int snarePitch[16];
int snareLength[16];
int snareLengthNoise[16];
int hhTone[16];
int hhLength[16];
int drumFilter[16];
int drumRes[16];

void setup() {

  AudioMemory(170);//orig 160; maxusage:165 //delay blocks (and others?) need memory. Each block holds 128 audio samples, or approx 2.9 ms of sound
  Serial.begin(115200);
  Serial.println("serial port open."); //mv

  //initialize haptic chip
  drv.begin();
  delay(20);
  drv.setMode(DRV2605_MODE_INTTRIG); // default, internal trigger when sending GO command
  drv.selectLibrary(1);
  drv.setWaveform(0, 84);  // ramp up medium 1, see datasheet part 11.2
  drv.setWaveform(1, 1);  // strong click 100%, see datasheet part 11.2
  drv.setWaveform(2, 0);  // end of waveforms

  //DAC
  sgtl5000_1.enable();
  sgtl5000_1.dacVolume(1);//mv pre DAC digital volume
  sgtl5000_1.lineOutLevel(13); //mv
  sgtl5000_1.unmuteLineout(); //mv
  sgtl5000_1.volume(.7); //mv headphone output
  sgtl5000_1.unmuteHeadphone();//mv add headphone output

  //Power Amp
  pinMode(5, OUTPUT);//mv
  digitalWrite(5, HIGH);//mv enable audio amp output
  //digitalWrite(5, LOW);//mv disable audio amp output

  //IMU
  imu.begin(); //mv enable IMU
  filter.begin(100); //mv enable IMU

  padOneWavOne.begin(.5,220,WAVEFORM_TRIANGLE);
  padOneWavTwo.begin(.5,220,WAVEFORM_TRIANGLE);
  padTwoWavOne.begin(.5,220,WAVEFORM_TRIANGLE);
  padTwoWavTwo.begin(.5,220,WAVEFORM_TRIANGLE);

  lfo.begin(0, 0, WAVEFORM_SAWTOOTH_REVERSE);
  lfoDC.amplitude(1);

  delayFilter.frequency(2000);
  delayFilter.resonance(.7);
  delay1.delay(0,0);
  delayMixStage.gain(3,0);

  oscStringMixer.gain(0, .3);
  oscStringMixerTwo.gain(0, .3);
  octave = 1;
  padOneFilter.octaveControl(2.5); //how much the lfo signal scales the filter frequency.
  padTwoFilter.octaveControl(2.5); //how much the lfo signal scales the filter frequency.
  lfoIndex = 2;
  drumLfoIndex = 2;
  firstRun = true;

  drumMixer.gain(0, .5); //kick
  drumMixer.gain(1, .1); //snare noise
  drumMixer.gain(2, .2); //snare
  drumMixer.gain(3, .4); //high hat
  kickFM.frequency(60); //the kick is mixed with a frequency modulated version of itself + pink noise;
  kickFM.amplitude(1);
  kickNoise.amplitude(.2); // the pink noise
  kickDrum.pitchMod(0.7); //the kick drum synth function parameters
  kickDrum.frequency(60); //the kick drum synth function parameters
  kickDrum.length(500); //the kick drum synth function parameters
  snare.amplitude(1);
  snareDrum.pitchMod(0.7);
  snareDrum.frequency(100);
  snareDrum.length(100);
  snareRelease = 500;
  hh.amplitude(1); //high hat is just band pass filtered white noise
  hhRelease = 20;
  hhFilter.frequency(9000);
  hhFilter.resonance(.7);
  mainDrumFilter.frequency(9000);
  mainDrumFilter.resonance(.7);
  mainDrumFilter.octaveControl(2.5);
  drumLFO.begin(0,0,WAVEFORM_SQUARE);
  seqPage = 0;
  seqStep = 0;
  prevSeqStep = 15;

  wavIndex = 0;

  //mv default drums sequence
  kickSteps[0] = true;
  kickSteps[4] = true;
  kickSteps[8] = true;
  kickSteps[12] = true;
  kickSteps[13] = true;
  snareSteps[2] = true;
  snareSteps[6] = true;
  snareSteps[10] = true;
  hhSteps[4] = true;
  hhSteps[8] = true;
  hhSteps[12] = true;
  hhSteps[14] = true;
  hhSteps[15] = true;
  seqInterval = 130;
  serialMetro.interval(seqInterval);

  Serial.println("setup complete."); //mv
}

int readXOne(){

//mv IMU note control begin
 if (gx > 20) {
  noteVal += 40;
 }
 if (gx < -20) {
  noteVal += -30;
 }
 if (noteVal > 1023) {
  noteVal=1023;
 }
 if (noteVal < 0) {
  noteVal=0;
 }

 if (gy > 40) {
  drumVol += .05;
 }
 if (gy < -30) {
  drumVol += -.05;
 }
 if (drumVol > 1) {
  drumVol=1;
 }
 if (drumVol < 0) {
  drumVol=0;
 }
 delayMixStage.gain(2, drumVol); //mv redirect drum volume

  return noteVal;
//mv IMU note control end
}

int readYOne(){
  return 512;//analogRead(A2); //martin edited to use single pot
}

int readXTwo(){
  return 0;//analogRead(A2); //martin edited to use single pot
}

int readYTwo(){
  return 512;//analogRead(A2); //martin edited to use single pot
}

int readMux(int channel){
  int val;

//mv hardcoded values instead of reading through the AMUX
  switch (channel) {
      case 12: //attack
          val = 512;
          break;
      case 11: //decay
          val = 512;
          break;
      case 10: //detune
          val = 512;
          break;
      case 9: //noise
          val = 0;
          break;
      case 8: //volume
          val = 1023;
          break;
      case 2: //filter
          val = 0;
          break;
      case 3: //resonance
          val = 512;
          break;
      case 4: //lfo freq
          val = 100;
          break;
      case 5: //lfo amp
          val = 0;
          break;
      case 6: //delay time
          //val = getSmooth(A3);
          val = 312;
          break;
      case 7: //delay gain
          val = touchAutorange(16);
          break;
      case 1: //octave dm: speed
          //val = getSmooth(A1);
          val = 500;
          break;
      case 0: //drum volume?
          val = 512;
          break;
      case 13: //drumMode
          val = LOW;
          break;
      case 14: //set the pad yposition to be either cutoff freq (HIGH) or detune source
          val = HIGH;
          break;
      default:
          val = 0;
          break;
  }
//Serial.print(" Ch"); //mv
//Serial.print(channel);
//Serial.print(":"); //mv
//Serial.print(val);

  //return the value
  return val;
}

float mapfloat(float x, float in_min, float in_max, float out_min, float out_max){
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

int getSmooth(int pin){
  int vals[10];
  for(int i = 0; i < 10; i++){
    vals[i] = analogRead(pin);
  }
  float smooth = (vals[0] + vals[1] + vals[2] + vals[3] + vals[4] + vals[5] + vals[6] + vals[7] + vals[8] + vals[9]) / 10;
  return smooth;
}

void setFreq(float val, float preVal, bool synthOne){
  if(synthOne){
      quantizedValue = map(val, 60, 750, 0, 3);
      val = aMinor[quantizedValue];
      prevQuantizedValue = map(preVal, 60, 750, 0, 3);
      preVal = aMinor[prevQuantizedValue];
  }else{
      quantizedValue = map(val, 60, 750, 4, 7);
      val = aMinor[quantizedValue];
      prevQuantizedValue = map(preVal, 60, 750, 4, 7);
      preVal = aMinor[prevQuantizedValue];
  }

  if(wavIndex == 3){  //STRING
      if(synthOne){
          if (abs(val - preVal) > 0 || !padOneNoteOn){
              string1.noteOn(val*detune*octave*yPosBendOne, .3);
              string2.noteOn(val*octave*yPosBendOne, .3);
          }
      }else{
          if (abs(val - preVal) > 0 || !padTwoNoteOn){
              string3.noteOn(val*detune*octave*yPosBendTwo, .3);
              string4.noteOn(val*octave*yPosBendTwo, .3);
          }
      }
  }else{  //NOT STRING
      //SET FREQ WITH GLIDE
      if(synthOne){
          padOneWavOne.frequency((val*detune)*octave*yPosBendOne);
          padOneWavTwo.frequency(val*octave*yPosBendOne);
      }
      if(!synthOne){
          padTwoWavOne.frequency((val*detune)*octave*yPosBendTwo);
          padTwoWavTwo.frequency(val*octave*yPosBendTwo);
      }
  }

//Serial.print("setFreq"); //mv
//Serial.print(val*detune*octave*yPosBendOne);
//Serial.print(" "); //mv
//Serial.println(val);
}

void loop() {
  //finalStage.gain(0,2); //mv volume blast
  seqPlay = true; //always play drums

  if (vibes < 10) {
    if (micros() > 1e6*vibes) {
      vibes++;
      drv.go();
      Serial.print("go ");
    }
  }

  //PAD ONE
  int x = readXOne();
  float y = readYOne();

  if((x < 1000) && (y < 1000)){
      padOneCurrentXVal = x;
      setFreq(padOneCurrentXVal, padOnePrevXVal, true);
      padOneNoteOn = true;
      padOnePrevXVal = padOneCurrentXVal;
  }else{
      padOneNoteOn = false;
  }

  if(padOneNoteOn){
      envOne.amplitude(.3,attackTime);
      if(yPosAlt){
          yPosBendOne = mapfloat(y, 125, 900, 1.05, .95);
      }else{
          padOneFilter.frequency(map(y, 125, 900, 4450, 1));
      }
  }else{
      envOne.amplitude(0, decayTime);
  }

  //PAD TWO
  int a = readXTwo();
  float b = readYTwo();

  if((a < 1000) && (b < 1000)){
      padTwoCurrentVal = a;
      setFreq(padTwoCurrentVal, padTwoPrevVal, false);
      padTwoNoteOn = true;
      padTwoPrevVal = padTwoCurrentVal;
  }else{
      padTwoNoteOn = false;
  }

  if(padTwoNoteOn){
      envTwo.amplitude(.3,attackTime);
      if(yPosAlt){
          yPosBendTwo = mapfloat(b, 125, 900, 1.05, .95);
      }else{
          padTwoFilter.frequency(map(b, 125, 900, 4450, 1));
      }
  }else{
      envTwo.amplitude(0, decayTime);
  }

  //mv begin add gyro control
  if (imu.available()) {
    // Read the motion sensors
    imu.readMotionSensor(ax, ay, az, gx, gy, gz, mx, my, mz);

    // Update the SensorFusion filter
    filter.update(gx, gy, gz, ax, ay, az, mx, my, mz);

    // print the heading, pitch and roll
    roll = filter.getRoll();
    pitch = filter.getPitch();
    heading = filter.getYaw();
    Serial.print("ax:");
    Serial.print(ax,1);
    Serial.print(" ay:");
    Serial.print(ay,1);
    Serial.print(" az:");
    Serial.println(az,1);

    Serial.print("gx:");
    Serial.print(gx,1);
    Serial.print(" gy:");
    Serial.print(gy,1);
    Serial.print(" gz:");
    Serial.println(gz,1);
    Serial.print(" heading: ");
    Serial.print(heading);
    Serial.print(" pitch: ");
    Serial.print(pitch);
    Serial.print(" roll: ");
    Serial.println(roll);
  }
//mv end gyro control block

//mv debug block
/*
    Serial.print("upRate:"); //mv debug
    Serial.print(micros()-mv_updatePrev); //mv debug
    mv_updatePrev = micros(); //mv debug
    Serial.print(" NoteOn:"); //mv debug
    Serial.print(padOneNoteOn); //mv debug
    Serial.print(" noteVal:"); //mv debug
    Serial.print(noteVal); //mv debug
    Serial.print(" Octave:"); //mv debug
    Serial.print(octave); //mv debug
//    Serial.print(" delayPot:"); //mv debug
//    Serial.print(analogValues[6]); //mv debug
//    Serial.print(" seqPlay:"); //mv debug
//    Serial.print(seqPlay); //mv debug
//    Serial.print(" seqInterval:"); //mv debug
//    Serial.print(seqInterval); //mv debug
    Serial.print(" x:"); //mv debug
    Serial.print(x); //mv debug
    Serial.print(" y:"); //mv debug
    Serial.print(y,0); //mv debug
    Serial.print(" wavIndex:"); //mv debug
    Serial.print(wavIndex); //mv debug
    Serial.print(" Mem:"); //mv debug
    Serial.print(AudioMemoryUsage()); //mv debug
    Serial.print("/"); //mv debug
    Serial.print(AudioMemoryUsageMax()); //mv debug
    Serial.print(" CPU:"); //mv debug
    Serial.print(AudioProcessorUsage()); //mv debug
    Serial.print("/"); //mv debug
    Serial.println(AudioProcessorUsageMax()); //mv debug
  }
*/

/* //mv attempt at profiling CPU usage of the code. not finished.
    Serial.print("\n");
    Serial.println(kickNoise.processorUsageMax());
    Serial.println(kickDrum.processorUsageMax());
    Serial.println(kickFM.processorUsageMax());
    Serial.println(snare.processorUsageMax());
    Serial.println(snareEnv.processorUsageMax());
    Serial.println(noise.processorUsageMax());
    Serial.println(hhEnv.processorUsageMax());
    Serial.println(kickPreMix.processorUsageMax());
    Serial.println(hh.processorUsageMax());
    Serial.println(padOneWavTwo.processorUsageMax());
    Serial.println(padOneWavOne.processorUsageMax());
    Serial.println(padTwoWavTwo.processorUsageMax());
    Serial.println(padTwoWavOne.processorUsageMax());
    Serial.println(kickEnv.processorUsageMax());
    Serial.println(snareDrum.processorUsageMax());
    Serial.println(snareDriver.processorUsageMax());
    Serial.println(hhDriver.processorUsageMax());
    Serial.println(kickMixer.processorUsageMax());
    Serial.println(string1.processorUsageMax());
    Serial.println(string2.processorUsageMax());
    Serial.println(string2.processorUsageMax());
    Serial.println(oscOneMixer.processorUsageMax());
    Serial.println(oscTwoMixer.processorUsageMax());
    Serial.println(string3.processorUsageMax());
    Serial.println(string4.processorUsageMax());
    Serial.println(hhFilter.processorUsageMax());
    Serial.println(envOne.processorUsageMax());
    Serial.println(envTwo.processorUsageMax());
    Serial.println(stringMixer.processorUsageMax());
    Serial.println(drumMixer.processorUsageMax());
    Serial.println(multiply1.processorUsageMax());
    Serial.println(multiply2.processorUsageMax());
    Serial.println(stringMixerTwo.processorUsageMax());
    Serial.println(lfo.processorUsageMax());
    Serial.println(drumLFO.processorUsageMax());
    Serial.println(lfoDC.processorUsageMax());
    Serial.println(oscStringMixerTwo.processorUsageMax());
    Serial.println(oscStringMixer.processorUsageMax());
    Serial.println(mainDrumFilter.processorUsageMax());
    Serial.println(lfoMixer.processorUsageMax());
    Serial.println(padOneFilter.processorUsageMax());
    Serial.println(lfoPeak.processorUsageMax());
    Serial.println(padTwoFilter.processorUsageMax());
    Serial.println(peak2.processorUsageMax());
    Serial.println(peak1.processorUsageMax());
    Serial.println(delayFilter.processorUsageMax());
    Serial.println(delayMixStage.processorUsageMax());
    Serial.println(delay1.processorUsageMax());
    Serial.println(finalStage.processorUsageMax());
    Serial.println(i2s2.processorUsageMax());
  */


  //PEAK DETECTION FOR LEDS
  if(peak1.available()){
      padOnePeak = (peak1.read()*2);
      padOnePeak = constrain(padOnePeak, 0, 1);
  }
  if(peak2.available()){
      padTwoPeak = (peak2.read()*2);
      padTwoPeak = constrain(padTwoPeak, 0, 1);
  }
  if(lfoPeak.available()){
      lfoPeakVal = lfoPeak.read();
  }

  //KNOBS//SWITCHES//BUTTON
  for(int i = 0; i < 16; i ++){ //cycle through all the knobs switches and buttons
      if(i < 13){ //the knobs
          analogValues[i] = readMux(i); //select that knob through the mux and read it
          if (abs(analogValues[i] - analogValuesLag[i]) > 10 || firstRun){ //mv default was 15
              switch (i) {
                  case 12: //attack
                      attackTime = analogValues[i]+1;
                      break;
                  case 11: //decay
                      decayTime = analogValues[i]+1;
                      break;
                  case 10: //detune
                      detune = mapfloat(analogValues[i], 0, 1023, .98, 1.02);
                      break;
                  case 9: //noise
                      noise.amplitude(analogValues[i]/2046);
                      break;
                  case 8: //volume
                      finalStage.gain(0,analogValues[i]/1023);
                      break;
                  case 2: //filter
                      padOneFilter.frequency(analogValues[i]*9);
                      padTwoFilter.frequency(analogValues[i]*9);
                      break;
                  case 3: //resonance
                      padOneFilter.resonance((analogValues[i]/260)+.7);
                      padTwoFilter.resonance((analogValues[i]/260)+.7);
                      break;
                  case 4: //lfo freq
                      if(seqPlay){
                          lfoIndex = analogValues[i]/204.6;
                          lfoIndex = constrain(lfoIndex, 0, 4);
                      }else{
                          lfo.frequency(analogValues[i]/50);
                      }
                      break;
                  case 5: //lfo amp
                      lfo.amplitude(analogValues[i]/1023);
                      break;
                  case 6: //delay time
                      delay1.delay(0, analogValues[i]/2.4); //sets delay in ms 1023=426ms. Each count of analogValues is 0.417ms
                      break;
                  case 7: //delay gain
                      delayMixStage.gain(3,analogValues[i]/1023);
                      break;
                  case 1: //octave knob
                      octIndex = analogValues[i]/204; //split the knob into 5 sections
                      octIndex = constrain(octIndex, 0, 4);
                      octave = octArray[octIndex];
                      break;
                  case 0: //drum volume?
                      //delayMixStage.gain(2, analogValues[i]/511); //mv redirect drum volume
                      //glideTime = analogValues[i]/2;
                      break;
                  default:
                      break;
              }
              analogValuesLag[i] = analogValues[i];
          }
      }else{
          digitalValues[i] = readMux(i);
          switch (i) {
              case 13: //drumMode
                  if(digitalValues[i] == LOW){
                      drumMode = false;
                  }else{
                      drumMode = true;
                  }
                  break;
              case 14: //set the pad yposition to be either cutoff freq or detune source
                  if(digitalValues[i] == LOW){
                      yPosAlt = true;
                  }else{
                      yPosAlt = false;
                      yPosBendOne = 1;
                      yPosBendTwo = 1;
                  }
                  break;
              case 15: //wave type push button
                  if (wavTrig.update()){
                      if (wavTrig.fallingEdge()){
                          wavIndex++;
                          if(wavIndex > 3){
                              wavIndex = 0;
                          }
                          padOneWavOne.begin(waveShapes[wavIndex]);
                          padOneWavTwo.begin(waveShapes[wavIndex]);
                          padTwoWavOne.begin(waveShapes[wavIndex]);
                          padTwoWavTwo.begin(waveShapes[wavIndex]);
                      }
                  }
                  break;
              default:
                  break;
          }
      }
  }

  //Mix between Drums and synth
  if(wavIndex == 3){
      //Mix between synth and string
      oscStringMixer.gain(0, 0);
      oscStringMixerTwo.gain(0, 0);
      oscStringMixer.gain(1, .3);
      oscStringMixerTwo.gain(1, .3);
  }else{
      oscStringMixer.gain(0, .3);
      oscStringMixerTwo.gain(0, .3);
      oscStringMixer.gain(1, 0);
      oscStringMixerTwo.gain(1, 0);
  }

  //DRUMS
  if(seqPlay){
      //Sync LFO
      lfo.frequency((1/seqInterval)*lfoTimeMathArray[lfoIndex]);
      drumLFO.frequency((1/seqInterval)*lfoTimeMathArray[drumLfoIndex]);

      if (serialMetro.check() == 1) { //if 500ms has elapsed
          seqStep++;
      }
      if (seqStep >= 16){
          seqStep = 0;
          lfo.phase(180);
          drumLFO.phase(180);
      }
      //Play da Drums
      if(abs(seqStep - prevSeqStep) > 0){
          if(kickSteps[seqStep]){
              kickDrum.noteOn();
          }
          if(snareSteps[seqStep]){
              snareEnv.amplitude(.5, 0);
              snareEnv.amplitude(0, snareRelease);
              snareDrum.noteOn();
          }
          if(hhSteps[seqStep]){
              hhEnv.amplitude(.5,0);
              hhEnv.amplitude(0,hhRelease);
          }
          //Drum Automation
          if(kickPitch[seqStep] > 0){
              kickDrum.frequency(kickPitch[seqStep]);
              kickFM.frequency(kickPitchFM[seqStep]);
          }
          if(kickLength[seqStep] > 0){
              kickDrum.length(kickLength[seqStep]);
          }
          if(snarePitch[seqStep] > 0){
              snareDrum.frequency(snarePitch[seqStep]);
          }
          if(snareLength[seqStep] > 0){
              snareDrum.length(snareLength[seqStep]);
              snareRelease = snareLength[seqStep];
          }
          if(hhTone[seqStep] > 0){
              hhFilter.frequency(hhTone[seqStep]);
          }
          if(hhLength[seqStep] > 0){
              hhRelease = hhLength[seqStep];
          }
          if(drumFilter[seqStep] > 0){
              mainDrumFilter.frequency(drumFilter[seqStep]);
          }
          if(drumRes[seqStep] > 0){
              mainDrumFilter.resonance(drumRes[seqStep]);
          }
      }
      prevSeqStep = seqStep;
  }
  firstRun = false;
}

//mv function to autorange cap pin readings
int touchAutorange(int pin){
  int sum = 0;

  if (maximum - baseline >= 1000) {
    baseline += 1; //mv the ranges drift closer to correct for environmental changes. Not very elegant.
    maximum += -1;
  } else {
    baseline += -20;
    maximum += 20;
  }
  for(int i = 0; i < 20; i++){ //averaging
    sum += touchRead(pin);
  }

  if (sum <= baseline) {
    baseline = sum;
  }

  if (sum >= maximum) {
    maximum = sum;
  }

//  Serial.print("sum:");
//  Serial.print(sum);
//  Serial.print(" baseline:");
//  Serial.print(baseline);
//  Serial.print(" maximum:");
//  Serial.print(maximum);
//  Serial.print(" return:");
//  Serial.println(round(mapfloat(sum, baseline, maximum, 0, 999)));
  return round(mapfloat(sum, baseline, maximum, 0, 999));
  }
